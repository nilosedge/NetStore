#include "config.h"
#include "func_db.h"
#include <iostream>
#include "DBThread.h"
#include <pcap.h>
#include "QueryNode.h"
#include <string>
#include "SniffStructs.h"

using namespace std;

DBThread *db = new DBThread(DEBUG);

static void Handler(u_char *ipaddrpoint, const struct pcap_pkthdr *p_header, const u_char *sp) { 
	const struct sniff_ethernet *ethernet;
	ethernet = (struct sniff_ethernet*)(sp);
	if(ethernet->ether_type != 8) return;
			
	char query[1000];
	unsigned short PacketLength = p_header->len - 14;
	unsigned int time =p_header->ts.tv_sec;
	unsigned int FromAddress = ((((int)sp[14+12]) << 24) + ((int)sp[14+13] << 16) + ((int)sp[14+14] << 8) + (int)sp[14+15]);
	unsigned int ToAddress =   ((((int)sp[14+16]) << 24) + ((int)sp[14+17] << 16) + ((int)sp[14+18] << 8) + (int)sp[14+19]);

	sprintf(query, "insert into `data_raw` (`from`, `to`, `amount`, time) values(\"%u\", \"%u\", \"%i\", from_unixtime(%i))", FromAddress, ToAddress, PacketLength, time);
	QueryNode *q = new QueryNode(query, INSERT_QUERY, true);
	db->Add(q);
}


int main(int argc, char **argv) {

	db->start();
	sleep(1);

	char errmsg[PCAP_ERRBUF_SIZE];
	pcap_t *pc;
	int val;
			
	pcap_handler fp = &Handler;

	pc = pcap_open_live(INTERFACE, 34, 1, 0, errmsg);
	if (pc == NULL) {
		puts(errmsg);
		_exit(1);
	}
			
	time_t t;
	time_t lt;
	time(&t);
	
	char consec[2000];
	char secdel[2000];
	char conmin[2000];
	char mindel[2000];
	char conhour[2000];
	char hourdel[2000];
	char conday[2000];
	char daydel[2000];
		
	while ((val = pcap_dispatch(pc, -1, fp, NULL ))>=0) {
		if (val < 0) break;
		time(&t);

		if(t != lt && ((t % 60) == 0)) {
			
			sprintf(consec, "insert into data_seconds select `from`, `to`, sum(amount) as amount, time from data_raw where time < ((from_unixtime(%i) - interval 10 minute)+0) group by `from`, `to`, time", (int)t);
			sprintf(secdel, "delete from data_raw where time < ((from_unixtime(%i) - interval 10 minute)+0)", (int)t);
			sprintf(conmin, "insert into data_minutes select `from`, `to`, sum(amount) as amount, concat(date(time), \" \", hour(time), \":\", minute(time), \":00\") as time2 from data_seconds where time < ((from_unixtime(%i) - interval 60 minute)+0) group by `from`, `to`, time2", (int)t);
			sprintf(mindel, "delete from data_seconds where time < ((from_unixtime(%i) - interval 60 minute)+0)", (int)t);
			sprintf(conhour, "insert into data_hours select `from`, `to`, sum(amount) as amount, concat(date(time), \" \", hour(time), \":00:00\") as time2 from data_minutes where time < ((from_unixtime(%i) - interval 2 day)+0) group by `from`, `to`, time2", (int)t);
			sprintf(hourdel, "delete from data_minutes where time < ((from_unixtime(%i) - interval 2 day)+0)", (int)t);
			sprintf(conday, "insert into data_daily select `from`, `to`, sum(amount) as amount, concat(date(time), \" 00:00:00\") as time2 from data_hours where time < ((from_unixtime(%i) - interval 80 day)+0) group by `from`, `to`, time2", (int)t);
			sprintf(daydel, "delete from data_hours where time < ((from_unixtime(%i) - interval 80 day)+0)", (int)t);
			
//			if(DEBUG) {
//				cout << consec << endl;
//				cout << secdel << endl;
//				cout << conmin << endl;
//				cout << mindel << endl;
//				cout << conhour << endl;
//				cout << hourdel << endl;
//				cout << conday << endl;
//				cout << daydel << endl;
//			}
						
			db->Add(new QueryNode(daydel, DELETE_QUERY, true));
			db->Add(new QueryNode(conday, INSERT_QUERY, true));
			db->Add(new QueryNode(hourdel, DELETE_QUERY, true));
			db->Add(new QueryNode(conhour, INSERT_QUERY, true));
			db->Add(new QueryNode(mindel, DELETE_QUERY, true));
			db->Add(new QueryNode(conmin, INSERT_QUERY, true));
			db->Add(new QueryNode(secdel, DELETE_QUERY, true));
			db->Add(new QueryNode(consec, INSERT_QUERY, true));
			
			lt = t;
		}
	}

	if (val < 0) puts(pcap_geterr(pc));
	pcap_close(pc);

}
