#ifndef DBThread_h_
#define DBThread_h_

//#include "Thread.h"
#include "config.h"
#include <cc++/thread.h>
#include <iostream>
#include "QueryNode.h"
#include "Queue.h"
#include "func_db.h"

	using namespace std;
	using namespace ost;

	class DBThread : public ost::Thread {
		public:
			DBThread(int _debug);
			~DBThread() {
				if(QueryQueue) delete QueryQueue;
				if(sem) delete sem;
				if(db) delete db;
			};
			virtual void run();
			void Add(QueryNode *node);
			void Finished();
			Queue *QueryQueue;
			func_db *db;

		private:
			ost::Semaphore *sem;
			int con;
			bool Running;
			int debug;
	};

#endif
