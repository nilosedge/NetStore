#include "QueryNode.h"

QueryNode::QueryNode(char *_Query, int _Type, bool _CanDelete = false) : Node() {
	CanDelete = _CanDelete;
	Type = _Type;
	Completed = false;
	if(strlen(_Query) > 0) {
		strcpy(Query, _Query);
	} else {
		cout << "Query: " << _Query << " is nothing.\n";
	}
}

void QueryNode::Print() {
	cout << "Type: " << Type << "Query: " << Query << endl;
	//cout << "Delete: " << CanDelete << " Completed: " << Completed << endl;
}

QueryNode::~QueryNode() {
	if(result) mysql_free_result(result);
}
