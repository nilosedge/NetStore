#include "DBThread.h"

DBThread::DBThread(int _debug) {
	debug = _debug;
	QueryQueue = new Queue(100000000);
	sem = new ost::Semaphore(1);
	db = new func_db(debug);


	if(!((con = db->connect(DBHOST, DBUSER, DBPASS)) > 0)) {
		cout << "Could not connect to db.\n";
		exit();
	}
	db->use_database(DBNAME);
	Running = true;
}

void DBThread::Add(QueryNode *node) {
	sem->wait();
	while(QueryQueue->Push(node) == false) { usleep(10); }
	sem->post();
}

void DBThread::Finished() {
	Running = false;
}


void DBThread::run() {

	//cout << "The db thread is start\n";

	while(Running || !QueryQueue->IsEmpty()) {

		if(!QueryQueue->IsEmpty()) {
			QueryNode *working = (QueryNode *)QueryQueue->Pop();
			//working->Print();
			if(!working->Query) {
				cout << "Problem with working->Query" << endl;
				cout << working->Type << endl;
			}
			int stln = strlen(working->Query);
			if(!(stln > 0)) {
				working->Print();
			}
			working->result = db->query(working->Query);
			if(working->CanDelete) { delete working; }
			else working->Completed = true;
		} else {
			usleep(10);
		}
	}
	cout << "DB Thread has finished." << endl;
	return;
}
