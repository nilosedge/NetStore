#include "func_db.h"

	func_db::func_db(int dbg = 1) {
		debug = dbg;
		if(debug) cout << "We are creating a DB object with debug level: " << debug << endl;
		mysql_init(&mysql);
		//if(debug) mysql_debug("d:t:O,/tmp/smbclient.trace");
		//sem = new ost::Semaphore(1);
	}

	int func_db::connect(const char* host, const char* username, const char*password) {
		if(!mysql_real_connect(&mysql,host,username,password, NULL, 0, 0, 0)) {
			if(debug) cout << "Failed to connect to host: " << host << " user: " << username << " password: " << password << endl;
			// func_db::error("MySQL down.", "\n\nCannot connect to mysql on" + host);
			return 0;
		}
		if(debug) cout << "We connected to the server " << host << " " << username << " " << password << endl;
		return 1;
	}

	int func_db::use_database(const char* db) {
		if(mysql_select_db(&mysql, db) < 0) {
			if(debug) { 
				cout << "Error in class Database: " << get_error() << endl;
				cout << "This is the db we are trying to connect to: " << db << endl;
			}
			return 0;
		} 
		if(debug) cout << "We are using the database " << db << endl;
		return 1;
	}

	MYSQL_RES * func_db::query(const char* query) {
		//sem->wait();
		MYSQL_RES *ret = NULL;
		if(debug) cout << "This is the query: " << query << endl;
		if(mysql_query(&mysql, query) != 0) {
			if(debug) cout << "Error in class Database: No record were found. No result_id\n";
		} else {
			ret = mysql_store_result(&mysql);
		}
		//sem->post();
		return ret;
	}

	reallong func_db::num_rows(MYSQL_RES *result) { return mysql_num_rows(result); }
	MYSQL_ROW func_db::get_row(MYSQL_RES *result) { return mysql_fetch_row(result); }
	void func_db::free_result(MYSQL_RES *result) { mysql_free_result(result); return; }
	unsigned long long func_db::get_index() { return mysql_insert_id(&mysql); }
	unsigned int func_db::num_fields(MYSQL_RES *result) { return mysql_num_fields(result); }
	unsigned long * func_db::get_len(MYSQL_RES *result) { return mysql_fetch_lengths(result); }
	const char * func_db::get_error() { return mysql_error(&mysql); }

