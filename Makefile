CC = c++
LIBS = -lmysqlclient_r -lccgnu2 -ldl -lpthread -lnsl -lresolv -lccgnu2 -lpthread -ldl -lpcap -L/usr/lib64/mysql
LIB = Node.o func_db.o DBThread.o Queue.o QueryNode.o
FLAGS = -Wall -pg -g
INC = -I/usr/include/cc++2 -I/usr/include/mysql3

all: NetStore

%.o: %.cpp %.h
	$(CC) $(FLAGS) $(INC) -c $<

NetStore: NetStore.cpp config.h $(LIB)
	$(CC) $(FLAGS) -o $@ $(LIB) $(LIBS) $(INC) $<

clean:
	rm -fr $(LIB)
	rm -fr NetStore
	rm -fr *.out
	rm -fr *.s
