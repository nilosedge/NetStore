#ifndef Node_h_
#define Node_h_

#include "config.h"
#include <iostream>

	using namespace std;

	class Node {
		public:
			Node(int id = 0);
			virtual void Print() = 0;
			virtual ~Node();
		public:
			int id;
			Node *Next, *Prev;
	};

#endif
