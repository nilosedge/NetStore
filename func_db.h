#ifndef func_db_h_
#define func_db_h_

#include <mysql/mysql.h>
#include <cc++/thread.h>
#include <iostream>

	typedef my_ulonglong reallong;

	using namespace std;
	using namespace ost;

	class func_db {

		public:
			func_db(int debug);
			int connect(const char* host, const char* username, const char*password);
			int use_database(const char* db_name);
			MYSQL_RES* query(const char* query);
			reallong num_rows(MYSQL_RES *result);
			MYSQL_ROW get_row(MYSQL_RES *result);
			unsigned long long get_index();
			void free_result(MYSQL_RES *result);
			unsigned long * get_len(MYSQL_RES *result);
			unsigned int num_fields(MYSQL_RES *result);
			const char * get_error();

		private:
			MYSQL mysql;
			int debug;
			ost::Semaphore *sem;

	};

#endif
