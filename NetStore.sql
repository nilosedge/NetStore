-- phpMyAdmin SQL Dump
-- version 2.6.4-pl4-Debian-2
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Dec 21, 2005 at 09:21 AM
-- Server version: 5.0.16
-- PHP Version: 4.4.0-4
-- 
-- Database: `netstore`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `data_daily`
-- 

DROP TABLE IF EXISTS `data_daily`;
CREATE TABLE IF NOT EXISTS `data_daily` (
  `from` int(10) unsigned NOT NULL default '0',
  `to` int(10) unsigned NOT NULL default '0',
  `amount` bigint(20) unsigned NOT NULL default '0',
  `time` timestamp NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  KEY `from` (`from`),
  KEY `to` (`to`),
  KEY `time` (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

-- 
-- Table structure for table `data_hours`
-- 

DROP TABLE IF EXISTS `data_hours`;
CREATE TABLE IF NOT EXISTS `data_hours` (
  `from` int(10) unsigned NOT NULL default '0',
  `to` int(10) unsigned NOT NULL default '0',
  `amount` bigint(20) unsigned NOT NULL default '0',
  `time` timestamp NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  KEY `from` (`from`),
  KEY `to` (`to`),
  KEY `time` (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

-- 
-- Table structure for table `data_minutes`
-- 

DROP TABLE IF EXISTS `data_minutes`;
CREATE TABLE IF NOT EXISTS `data_minutes` (
  `from` int(10) unsigned NOT NULL default '0',
  `to` int(10) unsigned NOT NULL default '0',
  `amount` bigint(20) unsigned NOT NULL default '0',
  `time` timestamp NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  KEY `from` (`from`),
  KEY `to` (`to`),
  KEY `time` (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

-- 
-- Table structure for table `data_raw`
-- 

DROP TABLE IF EXISTS `data_raw`;
CREATE TABLE IF NOT EXISTS `data_raw` (
  `from` int(10) unsigned NOT NULL default '0',
  `to` int(10) unsigned NOT NULL default '0',
  `amount` bigint(20) unsigned NOT NULL default '0',
  `time` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  KEY `from` (`from`),
  KEY `to` (`to`),
  KEY `time` (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

-- 
-- Table structure for table `data_seconds`
-- 

DROP TABLE IF EXISTS `data_seconds`;
CREATE TABLE IF NOT EXISTS `data_seconds` (
  `from` int(10) unsigned NOT NULL default '0',
  `to` int(10) unsigned NOT NULL default '0',
  `amount` bigint(20) unsigned NOT NULL default '0',
  `time` timestamp NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  KEY `from` (`from`),
  KEY `to` (`to`),
  KEY `time` (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
