#ifndef QueryNode_h_
#define QueryNode_h_

#include <iostream>
#include "config.h"
#include "Node.h"
#include <mysql/mysql.h>

using namespace std;

#define INSERT_QUERY 1
#define SELECT_QUERY 2
#define UPDATE_QUERY 3
#define DELETE_QUERY 4

class QueryNode : public Node {

	public:
		QueryNode(char *_Query, int _Type, bool _CanDelete);
		void Print();
		~QueryNode();

		volatile bool CanDelete;
		volatile bool Completed;
		volatile int Type;
		char Query[60000];
		MYSQL_RES *result;

};

#endif
