#include "Queue.h"
#include "Node.h"

Queue::Queue() {
	MaxSize = 10;
	CurrentSize = 0;
	Head = Tail = NULL;
	semfr = new Semaphore(1);
}

Queue::Queue(int _MaxSize) {
	MaxSize = _MaxSize;
	CurrentSize = 0;
	Head = Tail = NULL;
	semfr = new Semaphore(1);
}

bool Queue::Push(Node *InNode) {
	if(IsFull()) return false;
	semfr->wait();
	CurrentSize++;
	//InNode->Print();
	if(Head == NULL) {
		Head = InNode;
		Head->Next = NULL;
		Head->Prev = NULL;
		Tail = Head;
	} else {
		InNode->Prev = Tail;
		InNode->Next = NULL;
		Tail->Next = InNode;
		Tail = InNode;
	}
	//cout << "Size: " << CurrentSize << endl;
	//cout << "Full: " << IsFull() << endl;
	semfr->post();
	return true;
}

Node * Queue::Pop() {
	if(Head == NULL) return NULL;
	Node *T;
	semfr->wait();
	if(Head == Tail) {
		T = Head;
		Head = Tail = NULL;
	} else {
		T = Tail;
		Tail = Tail->Prev;
		Tail->Next = NULL;
	}
	CurrentSize--;
	T->Prev = T->Next = NULL;
	semfr->post();
	return T;
}

bool Queue::IsFull() {
	//cout << "Max: " << MaxSize << " Current: " << CurrentSize << endl;
	if(CurrentSize >= MaxSize) return true;
	return false;
}

bool Queue::IsEmpty() {
	if(CurrentSize == 0) return true;
	return false;
}

void Queue::Print() {
	cout << "Printing Queue\n";
	for(Node *T = Head; T != NULL; T = T->Next) {
		T->Print();
	}
}

Queue::~Queue() { 
	for(Node *T = Head; T != NULL;) {
		T = T->Next;
		delete T->Prev;
	}
	delete semfr;
	Head = Tail = NULL;
}
